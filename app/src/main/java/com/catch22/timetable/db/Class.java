package com.catch22.timetable.db;

public class Class
{
	public int id;
	public int credits;
	public String name;
	public String lecturer;
	public int colour;
	public String phoneNumber;

	public Class(int id, int credits, String name, String phoneNumber, int colour, String lecturer)
	{
		this.id = id;
		this.credits = credits;
		this.name = name;
		this.lecturer = lecturer;
		this.colour = colour;
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString()
	{
		return name;
	}
}

package com.catch22.timetable.db;

public class Lecture
{
	public int id;
	public int classId;
	public String startTime, endTime;
	public int weekNumber;

	public Lecture(int id, int classId, String startTime, String endTime, int weekNumber)
	{
		this.id = id;
		this.classId = classId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.weekNumber = weekNumber;
	}

	@Override
	public String toString()
	{
		return DatabaseHelper.getClass(classId) + " from " + startTime + " to " + endTime;
	}
}

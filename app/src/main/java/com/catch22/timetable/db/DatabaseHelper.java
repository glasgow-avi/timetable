package com.catch22.timetable.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;

import com.catch22.timetable.activities.DayviewActivity;

import java.util.ArrayList;
import java.util.Random;

import static com.catch22.timetable.db.DatabaseMaster.AttributeValuePair;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_COLOR;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_CREDIT;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_ID;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_LECTURER;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_PHONENUMBER;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_SUBJECT;
import static com.catch22.timetable.db.DatabaseMaster.CLASS_TABLE;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_CLASS;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_ENDTIME;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_ID;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_STARTTIME;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_TABLE;
import static com.catch22.timetable.db.DatabaseMaster.LECTURE_WEEK;

public class DatabaseHelper
{
	public static int addClass(String className, String lecturer, int credits, String phoneNumber) throws SQLiteConstraintException
	{
		DatabaseMaster db = DayviewActivity.db;

		db.insertInto(CLASS_TABLE,
		              new AttributeValuePair(CLASS_CREDIT, credits),
		              new AttributeValuePair(CLASS_LECTURER, lecturer),
		              new AttributeValuePair(CLASS_SUBJECT, className),
		              new AttributeValuePair(CLASS_COLOR, getColour()),
		              new AttributeValuePair(CLASS_PHONENUMBER, phoneNumber));

		Cursor c = db.getWritableDatabase().rawQuery("SELECT " + CLASS_ID +
				                                             " FROM " + CLASS_TABLE +
				                                             " WHERE " + CLASS_SUBJECT +
				                                             " = '" + className + "'",
		                                             null);
		int id = 0;
		while(c.moveToNext())
			id = c.getInt(0);

		return id;
	}

	public static Class getClass(int id)
	{
		Cursor c = DayviewActivity
				.db
				.getWritableDatabase()
				.query(CLASS_TABLE,
				       new String[]{CLASS_ID, CLASS_CREDIT, CLASS_SUBJECT, CLASS_PHONENUMBER, CLASS_COLOR, CLASS_LECTURER},
				       CLASS_ID + " = " + id, null, null, null, null);

		Class c1 = null;
		while(c.moveToNext())
			c1 = new Class(c.getInt(0), c.getInt(1), c.getString(2), c.getString(3), c.getInt(4), c.getString(5));

		return c1;
	}

	private static int getColour()
	{
		Random rand = new Random();
		int r = rand.nextInt(256);
		int g = rand.nextInt(256);
		int b = rand.nextInt(256);

		return Color.rgb(r, g, b);
	}

	public static void addLecture(String start, String end, int classNumber, int weekNumber) throws SQLiteConstraintException
	{
		DatabaseMaster db = DayviewActivity.db;

		db.insertInto(LECTURE_TABLE,
		              new AttributeValuePair(LECTURE_WEEK, weekNumber),
		              new AttributeValuePair(LECTURE_STARTTIME, start),
		              new AttributeValuePair(LECTURE_ENDTIME, end),
		              new AttributeValuePair(LECTURE_CLASS, classNumber));
	}

	public static void removeClass(int id)
	{
		SQLiteDatabase db = DayviewActivity.db.getWritableDatabase();

		db.execSQL("delete from " + LECTURE_TABLE +
				           " where " + LECTURE_CLASS + " = " + id);

		db.execSQL("delete from " + CLASS_TABLE +
				           " where " + CLASS_ID + " = " + id);
	}

	public static void removeLecture(int id)
	{
		SQLiteDatabase db = DayviewActivity.db.getWritableDatabase();

		db.execSQL("delete from " + LECTURE_TABLE +
				           " where " + LECTURE_ID + " = " + id);
	}

	public static Lecture[] getAllLecturesPerClass(int classId)
	{
		ArrayList<Lecture> accounts = new ArrayList<>();

		SQLiteDatabase db = DayviewActivity.db.getWritableDatabase();

		Cursor c = db.rawQuery("SELECT " + LECTURE_ID + ", " +
				                       LECTURE_STARTTIME + ", " +
				                       LECTURE_ENDTIME + ", " +
				                       LECTURE_WEEK +
				                       " FROM " + LECTURE_TABLE +
				                       " WHERE " + LECTURE_CLASS + " = " + classId, null);
		while(c.moveToNext())
			accounts.add(new Lecture(c.getInt(0), classId, c.getString(1), c.getString(2), c.getInt(3)));

		Lecture[] accountsArray = new Lecture[accounts.size()];

		for(int accountIndex = 0; accountIndex < accounts.size(); accountIndex++)
			accountsArray[accountIndex] = accounts.get(accountIndex);

		return accountsArray;
	}

	public static Lecture[] getAllLecturesPerWeek(int week)
	{
		ArrayList<Lecture> accounts = new ArrayList<>();

		SQLiteDatabase db = DayviewActivity.db.getWritableDatabase();

		Cursor c = db.rawQuery("SELECT " + LECTURE_ID + ", " +
				                       LECTURE_STARTTIME + ", " +
				                       LECTURE_ENDTIME + ", " +
				                       LECTURE_CLASS +
				                       " FROM " + LECTURE_TABLE +
									   " WHERE " + LECTURE_WEEK + " = " + week +
										" ORDER BY " + LECTURE_STARTTIME, null);

		while(c.moveToNext())
			accounts.add(new Lecture(c.getInt(0), c.getInt(3), c.getString(1), c.getString(2), week));

		Lecture[] accountsArray = new Lecture[accounts.size()];

		for(int accountIndex = 0; accountIndex < accounts.size(); accountIndex++)
			accountsArray[accountIndex] = accounts.get(accountIndex);

		return accountsArray;
	}
}

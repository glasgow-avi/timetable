package com.catch22.timetable.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseMaster extends SQLiteOpenHelper
{
	protected static class Column
	{
		public String name;
		public String dataType;
		public String constraints;

		public Column(String name, String dataType)
		{
			this.name = name;
			this.dataType = dataType;
			this.constraints = "";
		}

		public Column(String name, String dataType, String constraints)
		{
			this.name = name;
			this.dataType = dataType;
			this.constraints = constraints;
		}
	}

	protected static class AttributeValuePair
	{
		public String attribute;
		public Object value;

		public AttributeValuePair(String attribute, Object value)
		{
			this.attribute = attribute;
			this.value = value;
		}
	}

	public static final String DATABASE_NAME = "timetable.db";
	public static String LECTURE_TABLE = "Lectures",
			LECTURE_ID = "Id",
			LECTURE_CLASS = "Class_Id",
			LECTURE_STARTTIME = "Start_Time",
			LECTURE_ENDTIME = "End_Time",
			LECTURE_WEEK = "Week_No";

	public static String CLASS_TABLE = "Classes",
			CLASS_ID = "Id",
			CLASS_SUBJECT = "Subject",
			CLASS_CREDIT = "Credits",
			CLASS_COLOR = "ColouR",
			CLASS_LECTURER = "Lecturer",
			CLASS_PHONENUMBER = "Phone_No";

	public DatabaseMaster(Context context)
	{
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		onUpgrade(db, 1, 1);

		createTable(db, CLASS_TABLE,
		            new Column(CLASS_ID, "integer", "primary key"),
		            new Column(CLASS_SUBJECT, "text", "unique"),
		            new Column(CLASS_CREDIT, "integer"),
		            new Column(CLASS_COLOR, "integer"),
		            new Column(CLASS_LECTURER, "text"),
		            new Column(CLASS_PHONENUMBER, "text"));

		createTable(db, LECTURE_TABLE,
		            new Column(LECTURE_ID, "integer", "primary key"),
		            new Column(LECTURE_WEEK, "integer"),
		            new Column(LECTURE_CLASS, "integer"),
		            new Column(LECTURE_STARTTIME, "text"),
		            new Column(LECTURE_ENDTIME, "integer"));

		Log.d("SQL", "Table created");
	}

	private final void createTable(SQLiteDatabase db, String tableName, Column... columns)
	{
		String query = "create table " + tableName + "(";
		for(Column column : columns)
			query += column.name + " " + column.dataType + " " + column.constraints + ", ";

		query = query.substring(0, query.length() - 2);

		query += ");";

		Log.d("SQL", "Attempting query " + query);
		db.execSQL(query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + LECTURE_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + CLASS_TABLE);
	}

	public void insertInto(String tableName, AttributeValuePair... values)
	{
		ContentValues contentValues = new ContentValues();

		for(AttributeValuePair value : values)
			if(value.value instanceof String)
				contentValues.put(value.attribute, sanitiseString((String)value.value));
			else
				contentValues.put(value.attribute, (Integer) value.value);

		getWritableDatabase().insertOrThrow(tableName, null, contentValues);
	}

	public void deleteAllEntries()
	{
		getWritableDatabase().execSQL("delete from " + LECTURE_TABLE);
		getWritableDatabase().execSQL("delete from " + CLASS_TABLE);
	}

	public static String sanitiseString(String sqlQuery)
	{
		sqlQuery.replace(';', '\0');
		sqlQuery.replace("'", "''");

		return sqlQuery;
	}
}
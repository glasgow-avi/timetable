package com.catch22.timetable.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.catch22.timetable.R;

import static android.content.Context.MODE_PRIVATE;


public class AlarmReceiver extends BroadcastReceiver
{

	@RequiresApi (api = Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onReceive(Context context, Intent intent)
	{
		if(context.getSharedPreferences("timetable_pref", MODE_PRIVATE).getBoolean("notifications", true))
		{
			Notification.Builder notificationBuilder = new Notification.Builder(context);
			Notification notification = notificationBuilder
					.setContentTitle("Time table")
					.setContentText("You have class now")
					.setSmallIcon(R.mipmap.ic_launcher)
					.build();

			NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

			notificationManager.notify(1, notification);
		}
		if(context.getSharedPreferences("timetable_pref", MODE_PRIVATE).getBoolean("mute", true))
		{
			AudioManager amanager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
			amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
			amanager.setStreamMute(AudioManager.STREAM_ALARM, true);
			amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
			amanager.setStreamMute(AudioManager.STREAM_RING, true);
			amanager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
		}
	}
}

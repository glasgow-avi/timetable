package com.catch22.timetable.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public class ActivityFactory
{
	public static void navigate(final Activity source, final Class<?> destination)
	{
		Intent nextIntent = new Intent(source, destination);
		source.startActivity(nextIntent);
	}

	public static void setNavigation(View button, final Activity source, final Class<?> destination)
	{
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				navigate(source, destination);
			}
		});
	}
}

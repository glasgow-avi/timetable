package com.catch22.timetable.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.catch22.timetable.R;

public class Preferences extends AppCompatActivity
{
	CheckBox enableNotifications, enableMuting;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences);

		enableMuting = (CheckBox) findViewById(R.id.enableMutingPhone);
		enableNotifications = (CheckBox) findViewById(R.id.enableNotifications);

		enableNotifications.setChecked(getSharedPreferences("timetable_pref", MODE_PRIVATE).getBoolean("notifications", true));
		enableMuting.setChecked(getSharedPreferences("timetable_pref", MODE_PRIVATE).getBoolean("mute", true));

		final SharedPreferences.Editor editor = getSharedPreferences("timetable_pref", MODE_PRIVATE).edit();
		enableMuting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b)
			{
				editor.putBoolean("mute", b);
				editor.commit();
			}
		});

		enableNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b)
			{
				editor.putBoolean("notifications", b);
				editor.commit();
			}
		});
	}
}

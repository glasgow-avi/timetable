package com.catch22.timetable.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.catch22.timetable.R;
import com.catch22.timetable.db.DatabaseHelper;
import com.catch22.timetable.db.DatabaseMaster;
import com.catch22.timetable.db.Lecture;

import static com.catch22.timetable.db.DatabaseHelper.getAllLecturesPerWeek;

public class DayviewActivity extends AppCompatActivity
{
	public static String[] days;

	public static DatabaseMaster db;

	private static FloatingActionButton newClassButton;

	private static Lecture currentlySelectedLecture;
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link FragmentPagerAdapter} derivative, which will keep every
	 * loaded fragment in memory. If this becomes too memory intensive, it
	 * may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		if(db == null)
			db = new DatabaseMaster(this);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dayview);

		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		days = getResources().getStringArray(R.array.days);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager)findViewById(R.id.container);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(mViewPager);

		newClassButton = (FloatingActionButton)findViewById(R.id.newClassButton);
		ActivityFactory.setNavigation(newClassButton, this, AddClassesActivity.class);

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_dayview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if(id == R.id.action_settings)
		{
			ActivityFactory.navigate(this, Preferences.class);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		Log.d("Menu", "Context");
		switch(item.getItemId())
		{
			case R.id.popup_menu_delete:
				DatabaseHelper.removeLecture(currentlySelectedLecture.id);
				finish();
				ActivityFactory.navigate(DayviewActivity.this, DayviewActivity.class);
				break;
			case R.id.popup_menu_delete_class:
				DatabaseHelper.removeClass(currentlySelectedLecture.classId);
				finish();
				ActivityFactory.navigate(DayviewActivity.this, DayviewActivity.class);
				break;
			case R.id.popup_menu_call:
				String phoneNumber = DatabaseHelper.getClass(currentlySelectedLecture.classId).phoneNumber;
				if("".equals(phoneNumber))
				{
					Toast.makeText(this, getString(R.string.phone_number_not_saved), Toast.LENGTH_SHORT).show();
					break;
				}
				Intent call = new Intent(Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:" + phoneNumber));
				startActivity(call);
				break;
		}
		return false;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		getMenuInflater().inflate(R.menu.menu_context, menu);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class DayviewFragment extends Fragment
	{
		/**
		 * Classes of the day, list
		 */
		private static ListView listView;
		/**
		 * Adapter for list view.
		 */
		private static ArrayAdapter<Lecture> arrayAdapter;

		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		public static DayviewFragment newInstance(int sectionNumber)
		{
			DayviewFragment fragment = new DayviewFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
		                         Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_dayview, container, false);
			listView = (ListView)rootView.findViewById(R.id.listView);
			arrayAdapter = new ListViewAdapter(getContext(), R.layout.list_view, getAllLecturesPerWeek(getArguments().getInt(ARG_SECTION_NUMBER)));

			registerForContextMenu(listView);
			listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
			{
				@Override
				public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
				{
					currentlySelectedLecture =(Lecture)adapterView.getItemAtPosition(i);
					return false;
				}
			});

			listView.setAdapter(arrayAdapter);
			return rootView;
		}
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter
	{

		public SectionsPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			// getItem is called to instantiate the fragment for the given page.
			// Return a DayviewFragment (defined as a static inner class below).
			return DayviewFragment.newInstance(position + 1);
		}

		@Override
		public int getCount()
		{
			return days.length;
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			return days[position];
		}
	}
}
package com.catch22.timetable.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.catch22.timetable.R;
import com.catch22.timetable.db.Class;
import com.catch22.timetable.db.DatabaseHelper;
import com.catch22.timetable.notification.AlarmReceiver;

import java.util.ArrayList;
import java.util.Calendar;

public class AddClassesActivity extends AppCompatActivity
{
	private PendingIntent pendingIntent;
	private AlarmManager manager;
	@RequiresApi (api = Build.VERSION_CODES.JELLY_BEAN)
	private void scheduleNotification(String time)
	{
		Intent alarmIntent = new Intent(this, AlarmReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
		manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time.split(":")[0]));
		calendar.set(Calendar.MINUTE, Integer.valueOf(time.split(":")[1]));
		calendar.set(Calendar.SECOND, 0);
		long interval = 605000000;
		manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pendingIntent);
	}

	private GridLayout lectureEditorLayout;

	private class LectureEditor
	{
		private class TimePicker extends TimePickerDialog
		{
			public Button button;

			@Override
			public String toString()
			{
				return button.getText().toString();
			}

			public TimePicker(final Button b, final String title)
			{
				super(AddClassesActivity.this, new OnTimeSetListener()
				{
					@Override
					public void onTimeSet(android.widget.TimePicker timePicker, int i, int i1)
					{
						b.setText(String.format("%02d", i) + ":" + String.format("%02d", i1));
					}
				}, 0, 0, true);


				button = b;

				b.setText(title);
				b.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						show();
						setTitle(title);
					}
				});


			}
		}

		public TimePicker startTime;
		public TimePicker endTime;
		public Spinner week;
		public FloatingActionButton cancel;

		private View.OnClickListener cancelButtonListener = new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				lectureEditors.remove(LectureEditor.this);
				lectureEditorLayout.removeView(startTime.button);
				lectureEditorLayout.removeView(endTime.button);
				lectureEditorLayout.removeView(week);
				lectureEditorLayout.removeView(cancel);
			}
		};

		public LectureEditor()
		{
			startTime = new TimePicker(new Button(AddClassesActivity.this), getString(R.string.start));
			endTime = new TimePicker(new Button(AddClassesActivity.this), getString(R.string.end));
			week = new Spinner(AddClassesActivity.this);
			week.setAdapter(new ArrayAdapter<>(AddClassesActivity.this,
			                                   R.layout.support_simple_spinner_dropdown_item,
			                                   DayviewActivity.days)
			               );
			cancel = new FloatingActionButton(AddClassesActivity.this);
			cancel.setOnClickListener(cancelButtonListener);
			cancel.setSize(FloatingActionButton.SIZE_MINI);
			cancel.setImageDrawable(getDrawable(android.R.drawable.ic_delete));

			lectureEditorLayout.setColumnCount(4);
			lectureEditorLayout.addView(startTime.button);
			lectureEditorLayout.addView(endTime.button);
			lectureEditorLayout.addView(week);
			lectureEditorLayout.addView(cancel);
		}
	}

	private EditText phoneNumberEditText, subjectEditText, professorEditText, numberOfCreditsEditText;
	private FloatingActionButton addClassButton;

	private Button addLectureButton;

	public static Class activeClass = null;
	public static boolean editing = false;

	private ArrayList<LectureEditor> lectureEditors;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_classes);

		addLectureButton = (Button)findViewById(R.id.addLectureButton);
		lectureEditorLayout = (GridLayout)findViewById(R.id.addLectureLayout);

		lectureEditors = new ArrayList<>();

		addLectureButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				lectureEditors.add(new LectureEditor());
			}
		});

		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		phoneNumberEditText = (EditText) findViewById(R.id.phoneNumberEditText);
		subjectEditText = (EditText)findViewById(R.id.classNameEditText);
		professorEditText = (EditText)findViewById(R.id.professorNameEditText);
		numberOfCreditsEditText = (EditText)findViewById(R.id.noOfCreditsEditText);

		addClassButton = (FloatingActionButton)findViewById(R.id.addClassButton);
		addClassButton.setOnClickListener(new View.OnClickListener()
		{
			@RequiresApi (api = Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onClick(View view)
			{
				if("".equals(subjectEditText.getText().toString()) || "".equals(professorEditText.getText().toString()) || "".equals(numberOfCreditsEditText.getText().toString()))
				{
					Toast.makeText(AddClassesActivity.this, R.string.invalid_input, Toast.LENGTH_SHORT).show();
					return;
				}

				String phoneNumber = phoneNumberEditText.getText().toString();
				String subject = subjectEditText.getText().toString();
				String professor = professorEditText.getText().toString();
				Integer credits = Integer.valueOf(numberOfCreditsEditText.getText().toString());

				if(lectureEditors.size() == 0)
				{
					Toast.makeText(AddClassesActivity.this, R.string.no_lectures_added, Toast.LENGTH_SHORT).show();
					return;
				}

				for(LectureEditor lectureEditor : lectureEditors)
				{
					if(getString(R.string.start).equals(lectureEditor.startTime.toString()) || getString(R.string.end).equals(lectureEditor.endTime.toString()))
					{
						Toast.makeText(AddClassesActivity.this, getString(R.string.invalid_time), Toast.LENGTH_SHORT).show();
						return;
					}

					String startTime[] = lectureEditor.startTime.toString().split(":"), endTime[] = lectureEditor.endTime.toString().split(":");
					int startHour = Integer.valueOf(startTime[0]), startMinute = Integer.valueOf(startTime[1]);
					int endHour = Integer.valueOf(endTime[0]), endMinute = Integer.valueOf(endTime[1]);

					if(endHour * 1000 + endMinute < startHour * 1000 + startMinute)
					{
						Toast.makeText(AddClassesActivity.this, getString(R.string.invalid_time), Toast.LENGTH_SHORT).show();
						return;
					}
				}

				int classId;
				try
				{
					classId = DatabaseHelper.addClass(subject, professor, credits, phoneNumber);
				} catch (SQLiteConstraintException mvfc)
				{
					mvfc.printStackTrace();
					return;
				}

				for(LectureEditor lectureEditor : lectureEditors)
				{
					scheduleNotification(lectureEditor.startTime.toString());

					try
					{
						DatabaseHelper.addLecture(lectureEditor.startTime.toString(), lectureEditor.endTime.toString(),
						                          classId, lectureEditor.week.getSelectedItemPosition() + 1);
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				ActivityFactory.navigate(AddClassesActivity.this, DayviewActivity.class);
				finish();
			}
		});
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

}
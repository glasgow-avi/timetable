package com.catch22.timetable.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.catch22.timetable.R;
import com.catch22.timetable.db.Class;
import com.catch22.timetable.db.DatabaseHelper;
import com.catch22.timetable.db.Lecture;

public class ListViewAdapter extends ArrayAdapter<Lecture>
{
	Context mContext;
	int layoutResourceId;
	Lecture data[] = null;

	public ListViewAdapter(Context mContext, int layoutResourceId, Lecture[] data) {

		super(mContext, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@RequiresApi (api = Build.VERSION_CODES.M)
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if(convertView==null){
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}

		ImageView imageViewFolderIcon = (ImageView) convertView.findViewById(R.id.imageViewFolderIcon);
		TextView textViewFolderName = (TextView) convertView.findViewById(R.id.subjectName);
		TextView textViewFolderDescription = (TextView) convertView.findViewById(R.id.timingsText);
		TextView textViewTeacherName = (TextView) convertView.findViewById(R.id.teacherName);
		TextView textViewCredits = (TextView) convertView.findViewById(R.id.noOfCredits);

		Lecture folder = data[position];
		Class folderClass = DatabaseHelper.getClass(folder.classId);
		imageViewFolderIcon.setImageIcon(null);
		imageViewFolderIcon.setBackgroundColor(folderClass.colour);
		textViewFolderName.setText("  " + folderClass.name);
		textViewCredits.setText("   " + String.valueOf(folderClass.credits) + " " + getContext().getString(R.string.credits).split(" ")[getContext().getString(R.string.credits).split(" ").length - 1]);
		textViewFolderDescription.setText("   " + folder.startTime + " " + getContext().getString(R.string.to) + " " + folder.endTime);
		textViewTeacherName.setText("   " + folderClass.lecturer);

		return convertView;
	}
}
